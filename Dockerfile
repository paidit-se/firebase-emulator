FROM ubuntu:24.04@sha256:72297848456d5d37d1262630108ab308d3e9ec7ed1c3286a32fe09856619a782

WORKDIR /emulator

RUN apt-get update -y

RUN apt-get install -y curl openjdk-17-jre-headless

RUN curl -sL https://deb.nodesource.com/setup_20.x | bash - \
    && apt-get install -y nodejs

RUN npm install -g firebase-tools

ADD *.json *.rules .firebaserc /emulator/

RUN firebase setup:emulators:database
RUN firebase setup:emulators:firestore
RUN firebase setup:emulators:ui

ENTRYPOINT firebase emulators:start --import=/emulator/data/dump --project paidit-stage
